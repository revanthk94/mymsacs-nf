﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Models;


namespace MyMSACS.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        // public properties available from this context object
        // create a DbSet for each model

        // Model type is singular
        // DbSet name is plural

        public DbSet<DegreeStatus> DegreeStatuses { get; set; }
        public DbSet<RequirementStatus> RequirementStatuses { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<DegreeRequirement> DegreeRequirements { get; set; }
        public DbSet<StudentDegreePlan> StudentDegreePlans { get; set; }
        public DbSet<PlanTerm> PlanTerms { get; set; }
        public DbSet<PlanTermRequirement> PlanTermRequirements { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // by default, table names will match our set names which are plural. 
            // table names are typically singular, so override this default behavior
            // and name each table with singular

            // Create in a valid order - tables referenced must first exist

            builder.Entity<DegreeStatus>().ToTable("DegreeStatus");
            builder.Entity<RequirementStatus>().ToTable("RequirementStatus");
            builder.Entity<Degree>().ToTable("Degree");
            builder.Entity<Student>().ToTable("Student");

            builder.Entity<DegreeRequirement>().ToTable("DegreeRequirement");
            builder.Entity<StudentDegreePlan>().ToTable("StudentDegreePlan");
            builder.Entity<PlanTerm>().ToTable("PlanTerm");
            builder.Entity<PlanTermRequirement>().ToTable("PlanTermRequirement");
        }
    }
}